
# Group

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**login** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**reposUrl** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**membersUrl** | **String** |  |  [optional]
**description** | **String** |  |  [optional]



