package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * RepositoriesCommitCommitAuthor
 */

@javax.annotation.Generated(value = "io.swagger.generator.online.JavaClientCodegenImpl", date = "2017-11-16T14:06:19.500+08:00")

public class RepositoriesCommitCommitAuthor   {
  @SerializedName("name")
  private String name = null;

  @SerializedName("date")
  private java.util.Date date = null;

  @SerializedName("email")
  private String email = null;

  public RepositoriesCommitCommitAuthor name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "yu.wu", value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public RepositoriesCommitCommitAuthor date(java.util.Date date) {
    this.date = date;
    return this;
  }

   /**
   * Get date
   * @return date
  **/
  @ApiModelProperty(example = "2017-11-16T00:47:29+08:00", value = "")
  public java.util.Date getDate() {
    return date;
  }

  public void setDate(java.util.Date date) {
    this.date = date;
  }

  public RepositoriesCommitCommitAuthor email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(example = "191287278@qq.com", value = "")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RepositoriesCommitCommitAuthor repositoriesCommitCommitAuthor = (RepositoriesCommitCommitAuthor) o;
    return Objects.equals(this.name, repositoriesCommitCommitAuthor.name) &&
        Objects.equals(this.date, repositoriesCommitCommitAuthor.date) &&
        Objects.equals(this.email, repositoriesCommitCommitAuthor.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, date, email);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RepositoriesCommitCommitAuthor {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

