package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.Blob;
import com.gitee.api.model.Tree;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for GitDataApi
 */
public class GitDataApiTest {

    private GitDataApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(GitDataApi.class);
    }

    /**
     * 获取文件Blob
     *
     * 获取文件Blob
     */
    @Test
    public void getV5ReposOwnerRepoGitBlobsShaTest() {
        String owner = null;
        String repo = null;
        String sha = null;
        String accessToken = null;
        // Blob response = api.getV5ReposOwnerRepoGitBlobsSha(owner, repo, sha, accessToken);

        // TODO: test validations
    }
    /**
     * 获取目录Tree
     *
     * 获取目录Tree
     */
    @Test
    public void getV5ReposOwnerRepoGitTreesShaTest() {
        String owner = null;
        String repo = null;
        String sha = null;
        String accessToken = null;
        Integer recursive = null;
        // Tree response = api.getV5ReposOwnerRepoGitTreesSha(owner, repo, sha, accessToken, recursive);

        // TODO: test validations
    }
}
