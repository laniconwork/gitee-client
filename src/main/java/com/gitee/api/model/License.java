package com.gitee.api.model;

/**
 * created by wuyu on 2017/11/16
 */
public class License {

    private String license;

    private String source;

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
