# IssuesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoIssuesCommentsId**](IssuesApi.md#deleteV5ReposOwnerRepoIssuesCommentsId) | **DELETE** v5/repos/{owner}/{repo}/issues/comments/{id} | 删除Issue某条评论
[**getV5Issues**](IssuesApi.md#getV5Issues) | **GET** v5/issues | 获取当前授权用户的所有Issue
[**getV5OrgsOrgIssues**](IssuesApi.md#getV5OrgsOrgIssues) | **GET** v5/orgs/{org}/issues | 获取当前用户某个组织的Issues
[**getV5ReposOwnerRepoIssues**](IssuesApi.md#getV5ReposOwnerRepoIssues) | **GET** v5/repos/{owner}/{repo}/issues | 项目的所有Issues
[**getV5ReposOwnerRepoIssuesComments**](IssuesApi.md#getV5ReposOwnerRepoIssuesComments) | **GET** v5/repos/{owner}/{repo}/issues/comments | 获取项目所有Issue的评论
[**getV5ReposOwnerRepoIssuesCommentsId**](IssuesApi.md#getV5ReposOwnerRepoIssuesCommentsId) | **GET** v5/repos/{owner}/{repo}/issues/comments/{id} | 获取项目Issue某条评论
[**getV5ReposOwnerRepoIssuesNumber**](IssuesApi.md#getV5ReposOwnerRepoIssuesNumber) | **GET** v5/repos/{owner}/{repo}/issues/{number} | 项目的某个Issue
[**getV5ReposOwnerRepoIssuesNumberComments**](IssuesApi.md#getV5ReposOwnerRepoIssuesNumberComments) | **GET** v5/repos/{owner}/{repo}/issues/{number}/comments | 获取项目某个Issue所有的评论
[**getV5UserIssues**](IssuesApi.md#getV5UserIssues) | **GET** v5/user/issues | 获取当前授权用户的所有Issues
[**patchV5ReposOwnerRepoIssuesCommentsId**](IssuesApi.md#patchV5ReposOwnerRepoIssuesCommentsId) | **PATCH** v5/repos/{owner}/{repo}/issues/comments/{id} | 更新Issue某条评论
[**patchV5ReposOwnerRepoIssuesNumber**](IssuesApi.md#patchV5ReposOwnerRepoIssuesNumber) | **PATCH** v5/repos/{owner}/{repo}/issues/{number} | 更新Issue
[**postV5ReposOwnerRepoIssues**](IssuesApi.md#postV5ReposOwnerRepoIssues) | **POST** v5/repos/{owner}/{repo}/issues | 创建Issue
[**postV5ReposOwnerRepoIssuesNumberComments**](IssuesApi.md#postV5ReposOwnerRepoIssuesNumberComments) | **POST** v5/repos/{owner}/{repo}/issues/{number}/comments | 创建某个Issue评论


<a name="deleteV5ReposOwnerRepoIssuesCommentsId"></a>
# **deleteV5ReposOwnerRepoIssuesCommentsId**
> Void deleteV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken)

删除Issue某条评论

删除Issue某条评论

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5Issues"></a>
# **getV5Issues**
> java.util.List&lt;Issue&gt; getV5Issues(accessToken, filter, state, labels, sort, direction, since, page, perPage)

获取当前授权用户的所有Issue

获取当前授权用户的所有Issue

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
String filter = "assigned"; // String | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
String state = "open"; // String | Issue的状态: open, closed, or all。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Issue>> result = apiInstance.getV5Issues(accessToken, filter, state, labels, sort, direction, since, page, perPage);
result.subscribe(new Observer<java.util.List<Issue>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Issue> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **filter** | **String**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned] [enum: assigned, created, all]
 **state** | **String**| Issue的状态: open, closed, or all。 默认: open | [optional] [default to open] [enum: open, closed, all, started, approved]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5OrgsOrgIssues"></a>
# **getV5OrgsOrgIssues**
> java.util.List&lt;Issue&gt; getV5OrgsOrgIssues(org, accessToken, filter, state, labels, sort, direction, since, page, perPage)

获取当前用户某个组织的Issues

获取当前用户某个组织的Issues

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String filter = "assigned"; // String | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
String state = "open"; // String | Issue的状态: open, closed, or all。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Issue>> result = apiInstance.getV5OrgsOrgIssues(org, accessToken, filter, state, labels, sort, direction, since, page, perPage);
result.subscribe(new Observer<java.util.List<Issue>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Issue> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **filter** | **String**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned] [enum: assigned, created, all]
 **state** | **String**| Issue的状态: open, closed, or all。 默认: open | [optional] [default to open] [enum: open, closed, all, started, approved]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssues"></a>
# **getV5ReposOwnerRepoIssues**
> java.util.List&lt;Issue&gt; getV5ReposOwnerRepoIssues(owner, repo, accessToken, state, labels, sort, direction, since, page, perPage, milestone, assignee, creator)

项目的所有Issues

项目的所有Issues

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | Issue的状态: open, closed, or all。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
String milestone = "milestone_example"; // String | 根据里程碑标题。none为没里程碑的，*为所有带里程碑的
String assignee = "assignee_example"; // String | 用户的username。 none为没指派者, *为所有带有指派者的
String creator = "creator_example"; // String | 创建Issues的用户username
Observable<java.util.List<Issue>> result = apiInstance.getV5ReposOwnerRepoIssues(owner, repo, accessToken, state, labels, sort, direction, since, page, perPage, milestone, assignee, creator);
result.subscribe(new Observer<java.util.List<Issue>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Issue> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| Issue的状态: open, closed, or all。 默认: open | [optional] [default to open] [enum: open, closed, all, started, approved]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]
 **milestone** | **String**| 根据里程碑标题。none为没里程碑的，*为所有带里程碑的 | [optional]
 **assignee** | **String**| 用户的username。 none为没指派者, *为所有带有指派者的 | [optional]
 **creator** | **String**| 创建Issues的用户username | [optional]

### Return type

[**java.util.List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesComments"></a>
# **getV5ReposOwnerRepoIssuesComments**
> Void getV5ReposOwnerRepoIssuesComments(owner, repo, accessToken, sort, direction, since, page, perPage)

获取项目所有Issue的评论

获取项目所有Issue的评论

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "created"; // String | Either created or updated. Default: created
String direction = "asc"; // String | Either asc or desc. Ignored without the sort parameter.
String since = "since_example"; // String | Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<Void> result = apiInstance.getV5ReposOwnerRepoIssuesComments(owner, repo, accessToken, sort, direction, since, page, perPage);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| Either created or updated. Default: created | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| Either asc or desc. Ignored without the sort parameter. | [optional] [default to asc] [enum: asc, desc]
 **since** | **String**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesCommentsId"></a>
# **getV5ReposOwnerRepoIssuesCommentsId**
> Void getV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken)

获取项目Issue某条评论

获取项目Issue某条评论

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesNumber"></a>
# **getV5ReposOwnerRepoIssuesNumber**
> Issue getV5ReposOwnerRepoIssuesNumber(owner, repo, number, accessToken)

项目的某个Issue

项目的某个Issue

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Issue> result = apiInstance.getV5ReposOwnerRepoIssuesNumber(owner, repo, number, accessToken);
result.subscribe(new Observer<Issue>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Issue response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **String**| Issue 编号(区分大小写) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesNumberComments"></a>
# **getV5ReposOwnerRepoIssuesNumberComments**
> Void getV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, accessToken, since, page, perPage)

获取项目某个Issue所有的评论

获取项目某个Issue所有的评论

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写)
String accessToken = "accessToken_example"; // String | 用户授权码
String since = "since_example"; // String | Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<Void> result = apiInstance.getV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, accessToken, since, page, perPage);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **String**| Issue 编号(区分大小写) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **since** | **String**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserIssues"></a>
# **getV5UserIssues**
> java.util.List&lt;Issue&gt; getV5UserIssues(accessToken, filter, state, labels, sort, direction, since, page, perPage)

获取当前授权用户的所有Issues

获取当前授权用户的所有Issues

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
String filter = "assigned"; // String | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
String state = "open"; // String | Issue的状态: open, closed, or all。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Issue>> result = apiInstance.getV5UserIssues(accessToken, filter, state, labels, sort, direction, since, page, perPage);
result.subscribe(new Observer<java.util.List<Issue>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Issue> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **filter** | **String**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned] [enum: assigned, created, all]
 **state** | **String**| Issue的状态: open, closed, or all。 默认: open | [optional] [default to open] [enum: open, closed, all, started, approved]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoIssuesCommentsId"></a>
# **patchV5ReposOwnerRepoIssuesCommentsId**
> Void patchV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, body, accessToken)

更新Issue某条评论

更新Issue某条评论

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 评论的ID
String body = "body_example"; // String | The contents of the comment.
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.patchV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, body, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 评论的ID |
 **body** | **String**| The contents of the comment. |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoIssuesNumber"></a>
# **patchV5ReposOwnerRepoIssuesNumber**
> Issue patchV5ReposOwnerRepoIssuesNumber(owner, repo, number, title, accessToken, state, body, assignee, milestone, labels)

更新Issue

更新Issue

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写)
String title = "title_example"; // String | Issue标题
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "state_example"; // String | Issue 状态，open、started、closed 或 approved
String body = "body_example"; // String | Issue描述
String assignee = "assignee_example"; // String | Issue负责人的username
Integer milestone = 56; // Integer | 所属里程碑的number(第几个)
java.util.List<String> labels = Arrays.asList("labels_example"); // java.util.List<String> | 标签。如: [\"bug\",\"performance\"]。此处试验直接对标签名换行即可，如: bug performance
Observable<Issue> result = apiInstance.patchV5ReposOwnerRepoIssuesNumber(owner, repo, number, title, accessToken, state, body, assignee, milestone, labels);
result.subscribe(new Observer<Issue>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Issue response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **String**| Issue 编号(区分大小写) |
 **title** | **String**| Issue标题 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| Issue 状态，open、started、closed 或 approved | [optional] [enum: open, started, closed, approved]
 **body** | **String**| Issue描述 | [optional]
 **assignee** | **String**| Issue负责人的username | [optional]
 **milestone** | **Integer**| 所属里程碑的number(第几个) | [optional]
 **labels** | [**java.util.List&lt;String&gt;**](String.md)| 标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance | [optional]

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoIssues"></a>
# **postV5ReposOwnerRepoIssues**
> Issue postV5ReposOwnerRepoIssues(owner, repo, title, accessToken, body, assignee, milestone, labels)

创建Issue

创建Issue

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String title = "title_example"; // String | Issue标题
String accessToken = "accessToken_example"; // String | 用户授权码
String body = "body_example"; // String | Issue描述
String assignee = "assignee_example"; // String | Issue负责人的username
Integer milestone = 56; // Integer | 所属里程碑的number(第几个)
java.util.List<String> labels = Arrays.asList("labels_example"); // java.util.List<String> | 标签。如: [\"bug\",\"performance\"]。此处试验直接对标签名换行即可，如: bug performance
Observable<Issue> result = apiInstance.postV5ReposOwnerRepoIssues(owner, repo, title, accessToken, body, assignee, milestone, labels);
result.subscribe(new Observer<Issue>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Issue response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **title** | **String**| Issue标题 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **body** | **String**| Issue描述 | [optional]
 **assignee** | **String**| Issue负责人的username | [optional]
 **milestone** | **Integer**| 所属里程碑的number(第几个) | [optional]
 **labels** | [**java.util.List&lt;String&gt;**](String.md)| 标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance | [optional]

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoIssuesNumberComments"></a>
# **postV5ReposOwnerRepoIssuesNumberComments**
> Void postV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, body, accessToken)

创建某个Issue评论

创建某个Issue评论

### Example
```java
// Import classes:
//import com.gitee.api.api.IssuesApi;

IssuesApi apiInstance =  new ApiClient().create(IssuesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写)
String body = "body_example"; // String | The contents of the comment.
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.postV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, body, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **String**| Issue 编号(区分大小写) |
 **body** | **String**| The contents of the comment. |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

