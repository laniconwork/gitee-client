package com.gitee.api.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ProjectOwner
 */
public class ProjectOwner   {
  @SerializedName("login")
  private String login = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("avatar_url")
  private String avatarUrl = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("html_url")
  private String htmlUrl = null;

  @SerializedName("followers_url")
  private String followersUrl = null;

  @SerializedName("following_url")
  private String followingUrl = null;

  @SerializedName("gists_url")
  private String gistsUrl = null;

  @SerializedName("starred_url")
  private String starredUrl = null;

  @SerializedName("subscriptions_url")
  private String subscriptionsUrl = null;

  @SerializedName("organizations_url")
  private String organizationsUrl = null;

  @SerializedName("repos_url")
  private String reposUrl = null;

  @SerializedName("events_url")
  private String eventsUrl = null;

  @SerializedName("received_events_url")
  private String receivedEventsUrl = null;

  @SerializedName("type")
  private String type = null;

  @SerializedName("site_admin")
  private Boolean siteAdmin = null;

  public ProjectOwner login(String login) {
    this.login = login;
    return this;
  }

   /**
   * Get login
   * @return login
  **/
  @ApiModelProperty(example = "vanish136", value = "")
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public ProjectOwner id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "127095", value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public ProjectOwner avatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
    return this;
  }

   /**
   * Get avatarUrl
   * @return avatarUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/uploads/95/127095_vanish136.png?1478771060", value = "")
  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public ProjectOwner url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136", value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public ProjectOwner htmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
    return this;
  }

   /**
   * Get htmlUrl
   * @return htmlUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/vanish136", value = "")
  public String getHtmlUrl() {
    return htmlUrl;
  }

  public void setHtmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
  }

  public ProjectOwner followersUrl(String followersUrl) {
    this.followersUrl = followersUrl;
    return this;
  }

   /**
   * Get followersUrl
   * @return followersUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136/followers", value = "")
  public String getFollowersUrl() {
    return followersUrl;
  }

  public void setFollowersUrl(String followersUrl) {
    this.followersUrl = followersUrl;
  }

  public ProjectOwner followingUrl(String followingUrl) {
    this.followingUrl = followingUrl;
    return this;
  }

   /**
   * Get followingUrl
   * @return followingUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136/following_url{/other_user}", value = "")
  public String getFollowingUrl() {
    return followingUrl;
  }

  public void setFollowingUrl(String followingUrl) {
    this.followingUrl = followingUrl;
  }

  public ProjectOwner gistsUrl(String gistsUrl) {
    this.gistsUrl = gistsUrl;
    return this;
  }

   /**
   * Get gistsUrl
   * @return gistsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136/gists{/gist_id}", value = "")
  public String getGistsUrl() {
    return gistsUrl;
  }

  public void setGistsUrl(String gistsUrl) {
    this.gistsUrl = gistsUrl;
  }

  public ProjectOwner starredUrl(String starredUrl) {
    this.starredUrl = starredUrl;
    return this;
  }

   /**
   * Get starredUrl
   * @return starredUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136/starred{/owner}{/repo}", value = "")
  public String getStarredUrl() {
    return starredUrl;
  }

  public void setStarredUrl(String starredUrl) {
    this.starredUrl = starredUrl;
  }

  public ProjectOwner subscriptionsUrl(String subscriptionsUrl) {
    this.subscriptionsUrl = subscriptionsUrl;
    return this;
  }

   /**
   * Get subscriptionsUrl
   * @return subscriptionsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136/subscriptions", value = "")
  public String getSubscriptionsUrl() {
    return subscriptionsUrl;
  }

  public void setSubscriptionsUrl(String subscriptionsUrl) {
    this.subscriptionsUrl = subscriptionsUrl;
  }

  public ProjectOwner organizationsUrl(String organizationsUrl) {
    this.organizationsUrl = organizationsUrl;
    return this;
  }

   /**
   * Get organizationsUrl
   * @return organizationsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136/orgs", value = "")
  public String getOrganizationsUrl() {
    return organizationsUrl;
  }

  public void setOrganizationsUrl(String organizationsUrl) {
    this.organizationsUrl = organizationsUrl;
  }

  public ProjectOwner reposUrl(String reposUrl) {
    this.reposUrl = reposUrl;
    return this;
  }

   /**
   * Get reposUrl
   * @return reposUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136/repos", value = "")
  public String getReposUrl() {
    return reposUrl;
  }

  public void setReposUrl(String reposUrl) {
    this.reposUrl = reposUrl;
  }

  public ProjectOwner eventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
    return this;
  }

   /**
   * Get eventsUrl
   * @return eventsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136/events{/privacy}", value = "")
  public String getEventsUrl() {
    return eventsUrl;
  }

  public void setEventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
  }

  public ProjectOwner receivedEventsUrl(String receivedEventsUrl) {
    this.receivedEventsUrl = receivedEventsUrl;
    return this;
  }

   /**
   * Get receivedEventsUrl
   * @return receivedEventsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/vanish136/received_events", value = "")
  public String getReceivedEventsUrl() {
    return receivedEventsUrl;
  }

  public void setReceivedEventsUrl(String receivedEventsUrl) {
    this.receivedEventsUrl = receivedEventsUrl;
  }

  public ProjectOwner type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(example = "User", value = "")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public ProjectOwner siteAdmin(Boolean siteAdmin) {
    this.siteAdmin = siteAdmin;
    return this;
  }

   /**
   * Get siteAdmin
   * @return siteAdmin
  **/
  @ApiModelProperty(value = "")
  public Boolean isSiteAdmin() {
    return siteAdmin;
  }

  public void setSiteAdmin(Boolean siteAdmin) {
    this.siteAdmin = siteAdmin;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProjectOwner projectOwner = (ProjectOwner) o;
    return Objects.equals(this.login, projectOwner.login) &&
        Objects.equals(this.id, projectOwner.id) &&
        Objects.equals(this.avatarUrl, projectOwner.avatarUrl) &&
        Objects.equals(this.url, projectOwner.url) &&
        Objects.equals(this.htmlUrl, projectOwner.htmlUrl) &&
        Objects.equals(this.followersUrl, projectOwner.followersUrl) &&
        Objects.equals(this.followingUrl, projectOwner.followingUrl) &&
        Objects.equals(this.gistsUrl, projectOwner.gistsUrl) &&
        Objects.equals(this.starredUrl, projectOwner.starredUrl) &&
        Objects.equals(this.subscriptionsUrl, projectOwner.subscriptionsUrl) &&
        Objects.equals(this.organizationsUrl, projectOwner.organizationsUrl) &&
        Objects.equals(this.reposUrl, projectOwner.reposUrl) &&
        Objects.equals(this.eventsUrl, projectOwner.eventsUrl) &&
        Objects.equals(this.receivedEventsUrl, projectOwner.receivedEventsUrl) &&
        Objects.equals(this.type, projectOwner.type) &&
        Objects.equals(this.siteAdmin, projectOwner.siteAdmin);
  }

  @Override
  public int hashCode() {
    return Objects.hash(login, id, avatarUrl, url, htmlUrl, followersUrl, followingUrl, gistsUrl, starredUrl, subscriptionsUrl, organizationsUrl, reposUrl, eventsUrl, receivedEventsUrl, type, siteAdmin);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProjectOwner {\n");
    
    sb.append("    login: ").append(toIndentedString(login)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    avatarUrl: ").append(toIndentedString(avatarUrl)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
    sb.append("    followersUrl: ").append(toIndentedString(followersUrl)).append("\n");
    sb.append("    followingUrl: ").append(toIndentedString(followingUrl)).append("\n");
    sb.append("    gistsUrl: ").append(toIndentedString(gistsUrl)).append("\n");
    sb.append("    starredUrl: ").append(toIndentedString(starredUrl)).append("\n");
    sb.append("    subscriptionsUrl: ").append(toIndentedString(subscriptionsUrl)).append("\n");
    sb.append("    organizationsUrl: ").append(toIndentedString(organizationsUrl)).append("\n");
    sb.append("    reposUrl: ").append(toIndentedString(reposUrl)).append("\n");
    sb.append("    eventsUrl: ").append(toIndentedString(eventsUrl)).append("\n");
    sb.append("    receivedEventsUrl: ").append(toIndentedString(receivedEventsUrl)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    siteAdmin: ").append(toIndentedString(siteAdmin)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

