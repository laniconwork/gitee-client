package com.gitee.api.api;

import com.gitee.api.CollectionFormats.*;

import rx.Observable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import com.gitee.api.model.Blob;
import com.gitee.api.model.Tree;


public interface GitDataApi {
  /**
   * 获取文件Blob
   * 获取文件Blob
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param sha 文件Blob的SHA值 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Blob&gt;
   */
  @GET("v5/repos/{owner}/{repo}/git/blobs/{sha}")
  Observable<Blob> getV5ReposOwnerRepoGitBlobsSha(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("sha") String sha, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取目录Tree
   * 获取目录Tree
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param sha 可以是分支名(如master)、Commit或者目录Tree的SHA值 (required)
   * @param accessToken 用户授权码 (optional)
   * @param recursive 赋值为1递归获取目录 (optional)
   * @return Call&lt;Tree&gt;
   */
  @GET("v5/repos/{owner}/{repo}/git/trees/{sha}")
  Observable<Tree> getV5ReposOwnerRepoGitTreesSha(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("sha") String sha, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("recursive") Integer recursive
  );

}
