package com.gitee.api.api;

import com.gitee.api.CollectionFormats.*;

import rx.Observable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import com.gitee.api.model.SSHKey;
import com.gitee.api.model.SSHKeyBasic;
import com.gitee.api.model.User;
import com.gitee.api.model.UserAddress;
import com.gitee.api.model.UserBasic;
import com.gitee.api.model.UserDetail;
import com.gitee.api.model.UserEmail;


public interface UsersApi {
  /**
   * 取消关注一个用户
   * 取消关注一个用户
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/user/following/{username}")
  Observable<Void> deleteV5UserFollowingUsername(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 删除一个公钥
   * 删除一个公钥
   * @param id 公钥 ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/user/keys/{id}")
  Observable<Void> deleteV5UserKeysId(
    @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 删除授权用户未激活的邮箱地址
   * 删除授权用户未激活的邮箱地址
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/user/unconfirmed_email")
  Observable<Void> deleteV5UserUnconfirmedEmail(
    @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取授权用户的资料
   * 获取授权用户的资料
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;UserDetail&gt;
   */
  @GET("v5/user")
  Observable<UserDetail> getV5User(
    @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取授权用户的地理信息
   * 获取授权用户的地理信息
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;UserAddress&gt;
   */
  @GET("v5/user/address")
  Observable<UserAddress> getV5UserAddress(
    @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取授权用户的邮箱地址
   * 获取授权用户的邮箱地址
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;UserEmail&gt;
   */
  @GET("v5/user/emails")
  Observable<UserEmail> getV5UserEmails(
    @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出授权用户的关注者
   * 列出授权用户的关注者
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;UserBasic&gt;&gt;
   */
  @GET("v5/user/followers")
  Observable<java.util.List<UserBasic>> getV5UserFollowers(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出授权用户正关注的用户
   * 列出授权用户正关注的用户
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;UserBasic&gt;&gt;
   */
  @GET("v5/user/following")
  Observable<java.util.List<UserBasic>> getV5UserFollowing(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 检查授权用户是否关注了一个用户
   * 检查授权用户是否关注了一个用户
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @GET("v5/user/following/{username}")
  Observable<Void> getV5UserFollowingUsername(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出授权用户的所有公钥
   * 列出授权用户的所有公钥
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;SSHKey&gt;&gt;
   */
  @GET("v5/user/keys")
  Observable<java.util.List<SSHKey>> getV5UserKeys(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取一个公钥
   * 获取一个公钥
   * @param id 公钥 ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;SSHKey&gt;
   */
  @GET("v5/user/keys/{id}")
  Observable<SSHKey> getV5UserKeysId(
    @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取一个用户
   * 获取一个用户
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;User&gt;
   */
  @GET("v5/users/{username}")
  Observable<User> getV5UsersUsername(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出指定用户的关注者
   * 列出指定用户的关注者
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;UserBasic&gt;&gt;
   */
  @GET("v5/users/{username}/followers")
  Observable<java.util.List<UserBasic>> getV5UsersUsernameFollowers(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出指定用户正在关注的用户
   * 列出指定用户正在关注的用户
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;UserBasic&gt;&gt;
   */
  @GET("v5/users/{username}/following")
  Observable<java.util.List<UserBasic>> getV5UsersUsernameFollowing(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 检查指定用户是否关注目标用户
   * 检查指定用户是否关注目标用户
   * @param username 用户名(username/login) (required)
   * @param targetUser 目标用户的用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @GET("v5/users/{username}/following/{target_user}")
  Observable<Void> getV5UsersUsernameFollowingTargetUser(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Path("target_user") String targetUser, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出指定用户的所有公钥
   * 列出指定用户的所有公钥
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;SSHKeyBasic&gt;&gt;
   */
  @GET("v5/users/{username}/keys")
  Observable<java.util.List<SSHKeyBasic>> getV5UsersUsernameKeys(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 更新授权用户的资料
   * 更新授权用户的资料
   * @param accessToken 用户授权码 (optional)
   * @param name 昵称 (optional)
   * @param blog 微博链接 (optional)
   * @param weibo 博客站点 (optional)
   * @param bio 自我介绍 (optional)
   * @return Call&lt;UserDetail&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/user")
  Observable<UserDetail> patchV5User(
    @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("name") String name, @retrofit2.http.Field("blog") String blog, @retrofit2.http.Field("weibo") String weibo, @retrofit2.http.Field("bio") String bio
  );

  /**
   * 更新授权用户的地理信息
   * 更新授权用户的地理信息
   * @param accessToken 用户授权码 (optional)
   * @param name 联系人名 (optional)
   * @param tel 联系电话 (optional)
   * @param address 联系地址 (optional)
   * @param province 省份 (optional)
   * @param city 城市 (optional)
   * @param zipCode 邮政编码 (optional)
   * @param comment 备注 (optional)
   * @return Call&lt;UserDetail&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/user/address")
  Observable<UserDetail> patchV5UserAddress(
    @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("name") String name, @retrofit2.http.Field("tel") String tel, @retrofit2.http.Field("address") String address, @retrofit2.http.Field("province") String province, @retrofit2.http.Field("city") String city, @retrofit2.http.Field("zip_code") String zipCode, @retrofit2.http.Field("comment") String comment
  );

  /**
   * 添加授权用户的新邮箱地址
   * 添加授权用户的新邮箱地址
   * @param email 新的邮箱地址 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;UserEmail&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/user/emails")
  Observable<UserEmail> postV5UserEmails(
    @retrofit2.http.Field("email") String email, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 添加一个公钥
   * 添加一个公钥
   * @param key 公钥内容. (required)
   * @param title 公钥名称 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;SSHKey&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/user/keys")
  Observable<SSHKey> postV5UserKeys(
    @retrofit2.http.Field("key") String key, @retrofit2.http.Field("title") String title, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 关注一个用户
   * 关注一个用户
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/user/following/{username}")
  Observable<Void> putV5UserFollowingUsername(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Field("access_token") String accessToken
  );

}
