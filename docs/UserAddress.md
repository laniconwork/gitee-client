
# UserAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**tel** | **String** |  |  [optional]
**address** | **String** |  |  [optional]
**province** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**zipCode** | **String** |  |  [optional]
**comment** | **String** |  |  [optional]



