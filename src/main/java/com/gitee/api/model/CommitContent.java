package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * this is description
 */
public class CommitContent   {
  @SerializedName("content")
  private CommitContentContent content = null;

  @SerializedName("commit")
  private CommitContentCommit commit = null;

  public CommitContent content(CommitContentContent content) {
    this.content = content;
    return this;
  }

   /**
   * Get content
   * @return content
  **/
  public CommitContentContent getContent() {
    return content;
  }

  public void setContent(CommitContentContent content) {
    this.content = content;
  }

  public CommitContent commit(CommitContentCommit commit) {
    this.commit = commit;
    return this;
  }

   /**
   * Get commit
   * @return commit
  **/
  public CommitContentCommit getCommit() {
    return commit;
  }

  public void setCommit(CommitContentCommit commit) {
    this.commit = commit;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommitContent commitContent = (CommitContent) o;
    return Objects.equals(this.content, commitContent.content) &&
        Objects.equals(this.commit, commitContent.commit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(content, commit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CommitContent {\n");
    
    sb.append("    content: ").append(toIndentedString(content)).append("\n");
    sb.append("    commit: ").append(toIndentedString(commit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

