package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.Hook;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for WebhooksApi
 */
public class WebhooksApiTest {

    private WebhooksApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(WebhooksApi.class);
    }

    /**
     * 删除一个项目WebHook
     *
     * 删除一个项目WebHook
     */
    @Test
    public void deleteV5ReposOwnerRepoHooksIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoHooksId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 列出项目的WebHooks
     *
     * 列出项目的WebHooks
     */
    @Test
    public void getV5ReposOwnerRepoHooksTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Hook> response = api.getV5ReposOwnerRepoHooks(owner, repo, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取项目单个WebHook
     *
     * 获取项目单个WebHook
     */
    @Test
    public void getV5ReposOwnerRepoHooksIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Hook response = api.getV5ReposOwnerRepoHooksId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 更新一个项目WebHook
     *
     * 更新一个项目WebHook
     */
    @Test
    public void patchV5ReposOwnerRepoHooksIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String url = null;
        String accessToken = null;
        String password = null;
        Boolean pushEvents = null;
        Boolean tagPushEvents = null;
        Boolean issuesEvents = null;
        Boolean noteEvents = null;
        Boolean mergeRequestsEvents = null;
        // Hook response = api.patchV5ReposOwnerRepoHooksId(owner, repo, id, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents);

        // TODO: test validations
    }
    /**
     * 创建一个项目WebHook
     *
     * 创建一个项目WebHook
     */
    @Test
    public void postV5ReposOwnerRepoHooksTest() {
        String owner = null;
        String repo = null;
        String url = null;
        String accessToken = null;
        String password = null;
        Boolean pushEvents = null;
        Boolean tagPushEvents = null;
        Boolean issuesEvents = null;
        Boolean noteEvents = null;
        Boolean mergeRequestsEvents = null;
        // Hook response = api.postV5ReposOwnerRepoHooks(owner, repo, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents);

        // TODO: test validations
    }
    /**
     * 测试WebHook是否发送成功
     *
     * 测试WebHook是否发送成功
     */
    @Test
    public void postV5ReposOwnerRepoHooksIdTestsTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.postV5ReposOwnerRepoHooksIdTests(owner, repo, id, accessToken);

        // TODO: test validations
    }
}
