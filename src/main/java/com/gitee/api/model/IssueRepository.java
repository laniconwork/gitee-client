package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * IssueRepository
 */
public class IssueRepository {
    @SerializedName("id")
    private Integer id = null;

    @SerializedName("full_name")
    private String fullName = null;

    @SerializedName("url")
    private String url = null;

    @SerializedName("path")
    private String path = null;

    @SerializedName("name")
    private String name = null;

    @SerializedName("owner")
    private IssueRepositoryOwner owner = null;

    @SerializedName("description")
    private String description = null;

    @SerializedName("private")
    private Boolean _private = null;

    @SerializedName("fork")
    private Boolean fork = null;

    @SerializedName("html_url")
    private String htmlUrl = null;

    @SerializedName("forks_url")
    private String forksUrl = null;

    @SerializedName("keys_url")
    private String keysUrl = null;

    @SerializedName("collaborators_url")
    private String collaboratorsUrl = null;

    @SerializedName("hooks_url")
    private String hooksUrl = null;

    @SerializedName("branches_url")
    private String branchesUrl = null;

    @SerializedName("tags_url")
    private String tagsUrl = null;

    @SerializedName("blobs_url")
    private String blobsUrl = null;

    @SerializedName("stargazers_url")
    private String stargazersUrl = null;

    @SerializedName("contributors_url")
    private String contributorsUrl = null;

    @SerializedName("commits_url")
    private String commitsUrl = null;

    @SerializedName("comments_url")
    private String commentsUrl = null;

    @SerializedName("issue_comment_url")
    private String issueCommentUrl = null;

    @SerializedName("issues_url")
    private String issuesUrl = null;

    @SerializedName("pulls_url")
    private String pullsUrl = null;

    @SerializedName("milestones_url")
    private String milestonesUrl = null;

    @SerializedName("notifications_url")
    private String notificationsUrl = null;

    @SerializedName("labels_url")
    private String labelsUrl = null;

    @SerializedName("releases_url")
    private String releasesUrl = null;

    @SerializedName("recommend")
    private Boolean recommend = null;

    @SerializedName("homepage")
    private String homepage = null;

    @SerializedName("language")
    private String language = null;

    @SerializedName("forks_count")
    private Integer forksCount = null;

    @SerializedName("stargazers_count")
    private Integer stargazersCount = null;

    @SerializedName("watchers_count")
    private Integer watchersCount = null;

    @SerializedName("default_branch")
    private String defaultBranch = null;

    @SerializedName("open_issues_count")
    private Integer openIssuesCount = null;

    @SerializedName("has_issues")
    private Boolean hasIssues = null;

    @SerializedName("has_wiki")
    private Boolean hasWiki = null;

    @SerializedName("pull_requests_enabled")
    private Boolean pullRequestsEnabled = null;

    @SerializedName("has_page")
    private Boolean hasPage = null;

    @SerializedName("pushed_at")
    private java.util.Date pushedAt = null;

    @SerializedName("created_at")
    private java.util.Date createdAt = null;

    @SerializedName("updated_at")
    private java.util.Date updatedAt = null;

    public IssueRepository id(Integer id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IssueRepository fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    /**
     * Get fullName
     *
     * @return fullName
     **/
    @ApiModelProperty(example = "wuyu15255872976/spring-boot-starter-dubbo", value = "")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public IssueRepository url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     *
     * @return url
     **/
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public IssueRepository path(String path) {
        this.path = path;
        return this;
    }

    /**
     * Get path
     *
     * @return path
     **/
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public IssueRepository name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IssueRepository owner(IssueRepositoryOwner owner) {
        this.owner = owner;
        return this;
    }

    /**
     * Get owner
     *
     * @return owner
     **/
    @ApiModelProperty(value = "")
    public IssueRepositoryOwner getOwner() {
        return owner;
    }

    public void setOwner(IssueRepositoryOwner owner) {
        this.owner = owner;
    }

    public IssueRepository description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
     **/
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IssueRepository _private(Boolean _private) {
        this._private = _private;
        return this;
    }

    /**
     * Get _private
     *
     * @return _private
     **/
    @ApiModelProperty(value = "")
    public Boolean isPrivate() {
        return _private;
    }

    public void setPrivate(Boolean _private) {
        this._private = _private;
    }

    public IssueRepository fork(Boolean fork) {
        this.fork = fork;
        return this;
    }

    /**
     * Get fork
     *
     * @return fork
     **/
    @ApiModelProperty(value = "")
    public Boolean isFork() {
        return fork;
    }

    public void setFork(Boolean fork) {
        this.fork = fork;
    }

    public IssueRepository htmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
        return this;
    }

    /**
     * Get htmlUrl
     *
     * @return htmlUrl
     **/
    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public IssueRepository forksUrl(String forksUrl) {
        this.forksUrl = forksUrl;
        return this;
    }

    /**
     * Get forksUrl
     *
     * @return forksUrl
     **/
    public String getForksUrl() {
        return forksUrl;
    }

    public void setForksUrl(String forksUrl) {
        this.forksUrl = forksUrl;
    }

    public IssueRepository keysUrl(String keysUrl) {
        this.keysUrl = keysUrl;
        return this;
    }

    /**
     * Get keysUrl
     *
     * @return keysUrl
     **/
    public String getKeysUrl() {
        return keysUrl;
    }

    public void setKeysUrl(String keysUrl) {
        this.keysUrl = keysUrl;
    }

    public IssueRepository collaboratorsUrl(String collaboratorsUrl) {
        this.collaboratorsUrl = collaboratorsUrl;
        return this;
    }

    /**
     * Get collaboratorsUrl
     *
     * @return collaboratorsUrl
     **/
    public String getCollaboratorsUrl() {
        return collaboratorsUrl;
    }

    public void setCollaboratorsUrl(String collaboratorsUrl) {
        this.collaboratorsUrl = collaboratorsUrl;
    }

    public IssueRepository hooksUrl(String hooksUrl) {
        this.hooksUrl = hooksUrl;
        return this;
    }

    /**
     * Get hooksUrl
     *
     * @return hooksUrl
     **/
    public String getHooksUrl() {
        return hooksUrl;
    }

    public void setHooksUrl(String hooksUrl) {
        this.hooksUrl = hooksUrl;
    }

    public IssueRepository branchesUrl(String branchesUrl) {
        this.branchesUrl = branchesUrl;
        return this;
    }

    /**
     * Get branchesUrl
     *
     * @return branchesUrl
     **/
    public String getBranchesUrl() {
        return branchesUrl;
    }

    public void setBranchesUrl(String branchesUrl) {
        this.branchesUrl = branchesUrl;
    }

    public IssueRepository tagsUrl(String tagsUrl) {
        this.tagsUrl = tagsUrl;
        return this;
    }

    /**
     * Get tagsUrl
     *
     * @return tagsUrl
     **/
    public String getTagsUrl() {
        return tagsUrl;
    }

    public void setTagsUrl(String tagsUrl) {
        this.tagsUrl = tagsUrl;
    }

    public IssueRepository blobsUrl(String blobsUrl) {
        this.blobsUrl = blobsUrl;
        return this;
    }

    /**
     * Get blobsUrl
     *
     * @return blobsUrl
     **/
    public String getBlobsUrl() {
        return blobsUrl;
    }

    public void setBlobsUrl(String blobsUrl) {
        this.blobsUrl = blobsUrl;
    }

    public IssueRepository stargazersUrl(String stargazersUrl) {
        this.stargazersUrl = stargazersUrl;
        return this;
    }

    /**
     * Get stargazersUrl
     *
     * @return stargazersUrl
     **/
    public String getStargazersUrl() {
        return stargazersUrl;
    }

    public void setStargazersUrl(String stargazersUrl) {
        this.stargazersUrl = stargazersUrl;
    }

    public IssueRepository contributorsUrl(String contributorsUrl) {
        this.contributorsUrl = contributorsUrl;
        return this;
    }

    /**
     * Get contributorsUrl
     *
     * @return contributorsUrl
     **/
    public String getContributorsUrl() {
        return contributorsUrl;
    }

    public void setContributorsUrl(String contributorsUrl) {
        this.contributorsUrl = contributorsUrl;
    }

    public IssueRepository commitsUrl(String commitsUrl) {
        this.commitsUrl = commitsUrl;
        return this;
    }

    /**
     * Get commitsUrl
     *
     * @return commitsUrl
     **/
    public String getCommitsUrl() {
        return commitsUrl;
    }

    public void setCommitsUrl(String commitsUrl) {
        this.commitsUrl = commitsUrl;
    }

    public IssueRepository commentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
        return this;
    }

    /**
     * Get commentsUrl
     *
     * @return commentsUrl
     **/
    public String getCommentsUrl() {
        return commentsUrl;
    }

    public void setCommentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
    }

    public IssueRepository issueCommentUrl(String issueCommentUrl) {
        this.issueCommentUrl = issueCommentUrl;
        return this;
    }

    /**
     * Get issueCommentUrl
     *
     * @return issueCommentUrl
     **/
    public String getIssueCommentUrl() {
        return issueCommentUrl;
    }

    public void setIssueCommentUrl(String issueCommentUrl) {
        this.issueCommentUrl = issueCommentUrl;
    }

    public IssueRepository issuesUrl(String issuesUrl) {
        this.issuesUrl = issuesUrl;
        return this;
    }

    /**
     * Get issuesUrl
     *
     * @return issuesUrl
     **/
    public String getIssuesUrl() {
        return issuesUrl;
    }

    public void setIssuesUrl(String issuesUrl) {
        this.issuesUrl = issuesUrl;
    }

    public IssueRepository pullsUrl(String pullsUrl) {
        this.pullsUrl = pullsUrl;
        return this;
    }

    /**
     * Get pullsUrl
     *
     * @return pullsUrl
     **/
    public String getPullsUrl() {
        return pullsUrl;
    }

    public void setPullsUrl(String pullsUrl) {
        this.pullsUrl = pullsUrl;
    }

    public IssueRepository milestonesUrl(String milestonesUrl) {
        this.milestonesUrl = milestonesUrl;
        return this;
    }

    /**
     * Get milestonesUrl
     *
     * @return milestonesUrl
     **/
    public String getMilestonesUrl() {
        return milestonesUrl;
    }

    public void setMilestonesUrl(String milestonesUrl) {
        this.milestonesUrl = milestonesUrl;
    }

    public IssueRepository notificationsUrl(String notificationsUrl) {
        this.notificationsUrl = notificationsUrl;
        return this;
    }

    /**
     * Get notificationsUrl
     *
     * @return notificationsUrl
     **/
    public String getNotificationsUrl() {
        return notificationsUrl;
    }

    public void setNotificationsUrl(String notificationsUrl) {
        this.notificationsUrl = notificationsUrl;
    }

    public IssueRepository labelsUrl(String labelsUrl) {
        this.labelsUrl = labelsUrl;
        return this;
    }

    /**
     * Get labelsUrl
     *
     * @return labelsUrl
     **/
    public String getLabelsUrl() {
        return labelsUrl;
    }

    public void setLabelsUrl(String labelsUrl) {
        this.labelsUrl = labelsUrl;
    }

    public IssueRepository releasesUrl(String releasesUrl) {
        this.releasesUrl = releasesUrl;
        return this;
    }

    /**
     * Get releasesUrl
     *
     * @return releasesUrl
     **/
    public String getReleasesUrl() {
        return releasesUrl;
    }

    public void setReleasesUrl(String releasesUrl) {
        this.releasesUrl = releasesUrl;
    }

    public IssueRepository recommend(Boolean recommend) {
        this.recommend = recommend;
        return this;
    }

    /**
     * Get recommend
     *
     * @return recommend
     **/
    public Boolean isRecommend() {
        return recommend;
    }

    public void setRecommend(Boolean recommend) {
        this.recommend = recommend;
    }

    public IssueRepository homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    /**
     * Get homepage
     *
     * @return homepage
     **/
    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public IssueRepository language(String language) {
        this.language = language;
        return this;
    }

    /**
     * Get language
     *
     * @return language
     **/
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public IssueRepository forksCount(Integer forksCount) {
        this.forksCount = forksCount;
        return this;
    }

    /**
     * Get forksCount
     *
     * @return forksCount
     **/
    public Integer getForksCount() {
        return forksCount;
    }

    public void setForksCount(Integer forksCount) {
        this.forksCount = forksCount;
    }

    public IssueRepository stargazersCount(Integer stargazersCount) {
        this.stargazersCount = stargazersCount;
        return this;
    }

    /**
     * Get stargazersCount
     *
     * @return stargazersCount
     **/
    public Integer getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(Integer stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public IssueRepository watchersCount(Integer watchersCount) {
        this.watchersCount = watchersCount;
        return this;
    }

    /**
     * Get watchersCount
     *
     * @return watchersCount
     **/
    public Integer getWatchersCount() {
        return watchersCount;
    }

    public void setWatchersCount(Integer watchersCount) {
        this.watchersCount = watchersCount;
    }

    public IssueRepository defaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
        return this;
    }

    /**
     * Get defaultBranch
     *
     * @return defaultBranch
     **/
    public String getDefaultBranch() {
        return defaultBranch;
    }

    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    public IssueRepository openIssuesCount(Integer openIssuesCount) {
        this.openIssuesCount = openIssuesCount;
        return this;
    }

    /**
     * Get openIssuesCount
     *
     * @return openIssuesCount
     **/
    public Integer getOpenIssuesCount() {
        return openIssuesCount;
    }

    public void setOpenIssuesCount(Integer openIssuesCount) {
        this.openIssuesCount = openIssuesCount;
    }

    public IssueRepository hasIssues(Boolean hasIssues) {
        this.hasIssues = hasIssues;
        return this;
    }

    /**
     * Get hasIssues
     *
     * @return hasIssues
     **/
    public Boolean isHasIssues() {
        return hasIssues;
    }

    public void setHasIssues(Boolean hasIssues) {
        this.hasIssues = hasIssues;
    }

    public IssueRepository hasWiki(Boolean hasWiki) {
        this.hasWiki = hasWiki;
        return this;
    }

    /**
     * Get hasWiki
     *
     * @return hasWiki
     **/
    public Boolean isHasWiki() {
        return hasWiki;
    }

    public void setHasWiki(Boolean hasWiki) {
        this.hasWiki = hasWiki;
    }

    public IssueRepository pullRequestsEnabled(Boolean pullRequestsEnabled) {
        this.pullRequestsEnabled = pullRequestsEnabled;
        return this;
    }

    /**
     * Get pullRequestsEnabled
     *
     * @return pullRequestsEnabled
     **/
    public Boolean isPullRequestsEnabled() {
        return pullRequestsEnabled;
    }

    public void setPullRequestsEnabled(Boolean pullRequestsEnabled) {
        this.pullRequestsEnabled = pullRequestsEnabled;
    }

    public IssueRepository hasPage(Boolean hasPage) {
        this.hasPage = hasPage;
        return this;
    }

    /**
     * Get hasPage
     *
     * @return hasPage
     **/
    public Boolean isHasPage() {
        return hasPage;
    }

    public void setHasPage(Boolean hasPage) {
        this.hasPage = hasPage;
    }

    public IssueRepository pushedAt(java.util.Date pushedAt) {
        this.pushedAt = pushedAt;
        return this;
    }

    /**
     * Get pushedAt
     *
     * @return pushedAt
     **/
    public java.util.Date getPushedAt() {
        return pushedAt;
    }

    public void setPushedAt(java.util.Date pushedAt) {
        this.pushedAt = pushedAt;
    }

    public IssueRepository createdAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
     * Get createdAt
     *
     * @return createdAt
     **/
    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public IssueRepository updatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
     * Get updatedAt
     *
     * @return updatedAt
     **/
    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IssueRepository issueRepository = (IssueRepository) o;
        return Objects.equals(this.id, issueRepository.id) &&
                Objects.equals(this.fullName, issueRepository.fullName) &&
                Objects.equals(this.url, issueRepository.url) &&
                Objects.equals(this.path, issueRepository.path) &&
                Objects.equals(this.name, issueRepository.name) &&
                Objects.equals(this.owner, issueRepository.owner) &&
                Objects.equals(this.description, issueRepository.description) &&
                Objects.equals(this._private, issueRepository._private) &&
                Objects.equals(this.fork, issueRepository.fork) &&
                Objects.equals(this.htmlUrl, issueRepository.htmlUrl) &&
                Objects.equals(this.forksUrl, issueRepository.forksUrl) &&
                Objects.equals(this.keysUrl, issueRepository.keysUrl) &&
                Objects.equals(this.collaboratorsUrl, issueRepository.collaboratorsUrl) &&
                Objects.equals(this.hooksUrl, issueRepository.hooksUrl) &&
                Objects.equals(this.branchesUrl, issueRepository.branchesUrl) &&
                Objects.equals(this.tagsUrl, issueRepository.tagsUrl) &&
                Objects.equals(this.blobsUrl, issueRepository.blobsUrl) &&
                Objects.equals(this.stargazersUrl, issueRepository.stargazersUrl) &&
                Objects.equals(this.contributorsUrl, issueRepository.contributorsUrl) &&
                Objects.equals(this.commitsUrl, issueRepository.commitsUrl) &&
                Objects.equals(this.commentsUrl, issueRepository.commentsUrl) &&
                Objects.equals(this.issueCommentUrl, issueRepository.issueCommentUrl) &&
                Objects.equals(this.issuesUrl, issueRepository.issuesUrl) &&
                Objects.equals(this.pullsUrl, issueRepository.pullsUrl) &&
                Objects.equals(this.milestonesUrl, issueRepository.milestonesUrl) &&
                Objects.equals(this.notificationsUrl, issueRepository.notificationsUrl) &&
                Objects.equals(this.labelsUrl, issueRepository.labelsUrl) &&
                Objects.equals(this.releasesUrl, issueRepository.releasesUrl) &&
                Objects.equals(this.recommend, issueRepository.recommend) &&
                Objects.equals(this.homepage, issueRepository.homepage) &&
                Objects.equals(this.language, issueRepository.language) &&
                Objects.equals(this.forksCount, issueRepository.forksCount) &&
                Objects.equals(this.stargazersCount, issueRepository.stargazersCount) &&
                Objects.equals(this.watchersCount, issueRepository.watchersCount) &&
                Objects.equals(this.defaultBranch, issueRepository.defaultBranch) &&
                Objects.equals(this.openIssuesCount, issueRepository.openIssuesCount) &&
                Objects.equals(this.hasIssues, issueRepository.hasIssues) &&
                Objects.equals(this.hasWiki, issueRepository.hasWiki) &&
                Objects.equals(this.pullRequestsEnabled, issueRepository.pullRequestsEnabled) &&
                Objects.equals(this.hasPage, issueRepository.hasPage) &&
                Objects.equals(this.pushedAt, issueRepository.pushedAt) &&
                Objects.equals(this.createdAt, issueRepository.createdAt) &&
                Objects.equals(this.updatedAt, issueRepository.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, url, path, name, owner, description, _private, fork, htmlUrl, forksUrl, keysUrl, collaboratorsUrl, hooksUrl, branchesUrl, tagsUrl, blobsUrl, stargazersUrl, contributorsUrl, commitsUrl, commentsUrl, issueCommentUrl, issuesUrl, pullsUrl, milestonesUrl, notificationsUrl, labelsUrl, releasesUrl, recommend, homepage, language, forksCount, stargazersCount, watchersCount, defaultBranch, openIssuesCount, hasIssues, hasWiki, pullRequestsEnabled, hasPage, pushedAt, createdAt, updatedAt);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class IssueRepository {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("    path: ").append(toIndentedString(path)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    _private: ").append(toIndentedString(_private)).append("\n");
        sb.append("    fork: ").append(toIndentedString(fork)).append("\n");
        sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
        sb.append("    forksUrl: ").append(toIndentedString(forksUrl)).append("\n");
        sb.append("    keysUrl: ").append(toIndentedString(keysUrl)).append("\n");
        sb.append("    collaboratorsUrl: ").append(toIndentedString(collaboratorsUrl)).append("\n");
        sb.append("    hooksUrl: ").append(toIndentedString(hooksUrl)).append("\n");
        sb.append("    branchesUrl: ").append(toIndentedString(branchesUrl)).append("\n");
        sb.append("    tagsUrl: ").append(toIndentedString(tagsUrl)).append("\n");
        sb.append("    blobsUrl: ").append(toIndentedString(blobsUrl)).append("\n");
        sb.append("    stargazersUrl: ").append(toIndentedString(stargazersUrl)).append("\n");
        sb.append("    contributorsUrl: ").append(toIndentedString(contributorsUrl)).append("\n");
        sb.append("    commitsUrl: ").append(toIndentedString(commitsUrl)).append("\n");
        sb.append("    commentsUrl: ").append(toIndentedString(commentsUrl)).append("\n");
        sb.append("    issueCommentUrl: ").append(toIndentedString(issueCommentUrl)).append("\n");
        sb.append("    issuesUrl: ").append(toIndentedString(issuesUrl)).append("\n");
        sb.append("    pullsUrl: ").append(toIndentedString(pullsUrl)).append("\n");
        sb.append("    milestonesUrl: ").append(toIndentedString(milestonesUrl)).append("\n");
        sb.append("    notificationsUrl: ").append(toIndentedString(notificationsUrl)).append("\n");
        sb.append("    labelsUrl: ").append(toIndentedString(labelsUrl)).append("\n");
        sb.append("    releasesUrl: ").append(toIndentedString(releasesUrl)).append("\n");
        sb.append("    recommend: ").append(toIndentedString(recommend)).append("\n");
        sb.append("    homepage: ").append(toIndentedString(homepage)).append("\n");
        sb.append("    language: ").append(toIndentedString(language)).append("\n");
        sb.append("    forksCount: ").append(toIndentedString(forksCount)).append("\n");
        sb.append("    stargazersCount: ").append(toIndentedString(stargazersCount)).append("\n");
        sb.append("    watchersCount: ").append(toIndentedString(watchersCount)).append("\n");
        sb.append("    defaultBranch: ").append(toIndentedString(defaultBranch)).append("\n");
        sb.append("    openIssuesCount: ").append(toIndentedString(openIssuesCount)).append("\n");
        sb.append("    hasIssues: ").append(toIndentedString(hasIssues)).append("\n");
        sb.append("    hasWiki: ").append(toIndentedString(hasWiki)).append("\n");
        sb.append("    pullRequestsEnabled: ").append(toIndentedString(pullRequestsEnabled)).append("\n");
        sb.append("    hasPage: ").append(toIndentedString(hasPage)).append("\n");
        sb.append("    pushedAt: ").append(toIndentedString(pushedAt)).append("\n");
        sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
        sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

