
# PullRequestComments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**path** | **String** |  |  [optional]
**position** | **String** |  |  [optional]
**originalPosition** | **String** |  |  [optional]
**commitId** | **String** |  |  [optional]
**originalCommitId** | **String** |  |  [optional]
**user** | **String** |  |  [optional]
**body** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**pullRequestUrl** | **String** |  |  [optional]
**links** | **String** |  |  [optional]



