package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * RepositoriesCommitStats
 */

@javax.annotation.Generated(value = "io.swagger.generator.online.JavaClientCodegenImpl", date = "2017-11-16T14:06:19.500+08:00")

public class RepositoriesCommitStats   {
  @SerializedName("id")
  private String id = null;

  @SerializedName("additions")
  private Integer additions = null;

  @SerializedName("deletions")
  private Integer deletions = null;

  @SerializedName("total")
  private Integer total = null;

  public RepositoriesCommitStats id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "da3541317df136be1968ad45d7bd0237d90470c8", value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public RepositoriesCommitStats additions(Integer additions) {
    this.additions = additions;
    return this;
  }

   /**
   * Get additions
   * @return additions
  **/
  @ApiModelProperty(example = "2560", value = "")
  public Integer getAdditions() {
    return additions;
  }

  public void setAdditions(Integer additions) {
    this.additions = additions;
  }

  public RepositoriesCommitStats deletions(Integer deletions) {
    this.deletions = deletions;
    return this;
  }

   /**
   * Get deletions
   * @return deletions
  **/
  @ApiModelProperty(example = "68", value = "")
  public Integer getDeletions() {
    return deletions;
  }

  public void setDeletions(Integer deletions) {
    this.deletions = deletions;
  }

  public RepositoriesCommitStats total(Integer total) {
    this.total = total;
    return this;
  }

   /**
   * Get total
   * @return total
  **/
  @ApiModelProperty(example = "2628", value = "")
  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RepositoriesCommitStats repositoriesCommitStats = (RepositoriesCommitStats) o;
    return Objects.equals(this.id, repositoriesCommitStats.id) &&
        Objects.equals(this.additions, repositoriesCommitStats.additions) &&
        Objects.equals(this.deletions, repositoriesCommitStats.deletions) &&
        Objects.equals(this.total, repositoriesCommitStats.total);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, additions, deletions, total);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RepositoriesCommitStats {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    additions: ").append(toIndentedString(additions)).append("\n");
    sb.append("    deletions: ").append(toIndentedString(deletions)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

