package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.Event;
import com.gitee.api.model.Project;
import com.gitee.api.model.UserBasic;
import com.gitee.api.model.UserMessage;
import com.gitee.api.model.UserNotification;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for ActivityApi
 */
public class ActivityApiTest {

    private ActivityApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(ActivityApi.class);
    }

    /**
     * 取消 star 一个项目
     *
     * 取消 star 一个项目
     */
    @Test
    public void deleteV5UserStarredOwnerRepoTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.deleteV5UserStarredOwnerRepo(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 取消 watch 一个项目
     *
     * 取消 watch 一个项目
     */
    @Test
    public void deleteV5UserSubscriptionsOwnerRepoTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.deleteV5UserSubscriptionsOwnerRepo(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 获取站内所有公开动态
     *
     * 获取站内所有公开动态
     */
    @Test
    public void getV5EventsTest() {
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Event> response = api.getV5Events(accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出项目的所有公开动态
     *
     * 列出项目的所有公开动态
     */
    @Test
    public void getV5NetworksOwnerRepoEventsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Event> response = api.getV5NetworksOwnerRepoEvents(owner, repo, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出授权用户的所有私信
     *
     * 列出授权用户的所有私信
     */
    @Test
    public void getV5NotificationsMessagesTest() {
        String accessToken = null;
        Boolean unread = null;
        String since = null;
        String before = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<UserMessage> response = api.getV5NotificationsMessages(accessToken, unread, since, before, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取一个私信
     *
     * 获取一个私信
     */
    @Test
    public void getV5NotificationsMessagesIdTest() {
        Integer id = null;
        String accessToken = null;
        // UserMessage response = api.getV5NotificationsMessagesId(id, accessToken);

        // TODO: test validations
    }
    /**
     * 列出授权用户的所有通知
     *
     * 列出授权用户的所有通知
     */
    @Test
    public void getV5NotificationsThreadsTest() {
        String accessToken = null;
        Boolean unread = null;
        Boolean participating = null;
        String since = null;
        String before = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<UserNotification> response = api.getV5NotificationsThreads(accessToken, unread, participating, since, before, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取一个通知
     *
     * 获取一个通知
     */
    @Test
    public void getV5NotificationsThreadsIdTest() {
        Integer id = null;
        String accessToken = null;
        // UserNotification response = api.getV5NotificationsThreadsId(id, accessToken);

        // TODO: test validations
    }
    /**
     * 列出组织的公开动态
     *
     * 列出组织的公开动态
     */
    @Test
    public void getV5OrgsOrgEventsTest() {
        String org = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Event> response = api.getV5OrgsOrgEvents(org, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出项目的所有动态
     *
     * 列出项目的所有动态
     */
    @Test
    public void getV5ReposOwnerRepoEventsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Event> response = api.getV5ReposOwnerRepoEvents(owner, repo, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出一个项目里的通知
     *
     * 列出一个项目里的通知
     */
    @Test
    public void getV5ReposOwnerRepoNotificationsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        Boolean unread = null;
        Boolean participating = null;
        String since = null;
        String before = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<UserNotification> response = api.getV5ReposOwnerRepoNotifications(owner, repo, accessToken, unread, participating, since, before, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出 star 了项目的用户
     *
     * 列出 star 了项目的用户
     */
    @Test
    public void getV5ReposOwnerRepoStargazersTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<UserBasic> response = api.getV5ReposOwnerRepoStargazers(owner, repo, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出 watch 了项目的用户
     *
     * 列出 watch 了项目的用户
     */
    @Test
    public void getV5ReposOwnerRepoSubscribersTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<UserBasic> response = api.getV5ReposOwnerRepoSubscribers(owner, repo, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出授权用户 star 了的项目
     *
     * 列出授权用户 star 了的项目
     */
    @Test
    public void getV5UserStarredTest() {
        String accessToken = null;
        String sort = null;
        String direction = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Project> response = api.getV5UserStarred(accessToken, sort, direction, page, perPage);

        // TODO: test validations
    }
    /**
     * 检查授权用户是否 star 了一个项目
     *
     * 检查授权用户是否 star 了一个项目
     */
    @Test
    public void getV5UserStarredOwnerRepoTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.getV5UserStarredOwnerRepo(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 列出授权用户 watch 了的项目
     *
     * 列出授权用户 watch 了的项目
     */
    @Test
    public void getV5UserSubscriptionsTest() {
        String accessToken = null;
        String sort = null;
        String direction = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Project> response = api.getV5UserSubscriptions(accessToken, sort, direction, page, perPage);

        // TODO: test validations
    }
    /**
     * 检查授权用户是否 watch 了一个项目
     *
     * 检查授权用户是否 watch 了一个项目
     */
    @Test
    public void getV5UserSubscriptionsOwnerRepoTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.getV5UserSubscriptionsOwnerRepo(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 列出用户的动态
     *
     * 列出用户的动态
     */
    @Test
    public void getV5UsersUsernameEventsTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Event> response = api.getV5UsersUsernameEvents(username, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出用户所属组织的动态
     *
     * 列出用户所属组织的动态
     */
    @Test
    public void getV5UsersUsernameEventsOrgsOrgTest() {
        String username = null;
        String org = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Event> response = api.getV5UsersUsernameEventsOrgsOrg(username, org, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出用户的公开动态
     *
     * 列出用户的公开动态
     */
    @Test
    public void getV5UsersUsernameEventsPublicTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Event> response = api.getV5UsersUsernameEventsPublic(username, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出一个用户收到的动态
     *
     * 列出一个用户收到的动态
     */
    @Test
    public void getV5UsersUsernameReceivedEventsTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Event> response = api.getV5UsersUsernameReceivedEvents(username, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出一个用户收到的公开动态
     *
     * 列出一个用户收到的公开动态
     */
    @Test
    public void getV5UsersUsernameReceivedEventsPublicTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Event> response = api.getV5UsersUsernameReceivedEventsPublic(username, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出用户 star 了的项目
     *
     * 列出用户 star 了的项目
     */
    @Test
    public void getV5UsersUsernameStarredTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        String sort = null;
        String direction = null;
        // java.util.List<Project> response = api.getV5UsersUsernameStarred(username, accessToken, page, perPage, sort, direction);

        // TODO: test validations
    }
    /**
     * 列出用户 watch 了的项目
     *
     * 列出用户 watch 了的项目
     */
    @Test
    public void getV5UsersUsernameSubscriptionsTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        String sort = null;
        String direction = null;
        // java.util.List<Project> response = api.getV5UsersUsernameSubscriptions(username, accessToken, page, perPage, sort, direction);

        // TODO: test validations
    }
    /**
     * 标记一个私信为已读
     *
     * 标记一个私信为已读
     */
    @Test
    public void patchV5NotificationsMessagesIdTest() {
        Integer id = null;
        String accessToken = null;
        // Void response = api.patchV5NotificationsMessagesId(id, accessToken);

        // TODO: test validations
    }
    /**
     * 标记一个通知为已读
     *
     * 标记一个通知为已读
     */
    @Test
    public void patchV5NotificationsThreadsIdTest() {
        Integer id = null;
        String accessToken = null;
        // Void response = api.patchV5NotificationsThreadsId(id, accessToken);

        // TODO: test validations
    }
    /**
     * 发送私信给指定用户
     *
     * 发送私信给指定用户
     */
    @Test
    public void postV5NotificationsMessagesTest() {
        String username = null;
        String content = null;
        String accessToken = null;
        // Void response = api.postV5NotificationsMessages(username, content, accessToken);

        // TODO: test validations
    }
    /**
     * 标记所有私信为已读
     *
     * 标记所有私信为已读
     */
    @Test
    public void putV5NotificationsMessagesTest() {
        String accessToken = null;
        // Void response = api.putV5NotificationsMessages(accessToken);

        // TODO: test validations
    }
    /**
     * 标记所有通知为已读
     *
     * 标记所有通知为已读
     */
    @Test
    public void putV5NotificationsThreadsTest() {
        String accessToken = null;
        // Void response = api.putV5NotificationsThreads(accessToken);

        // TODO: test validations
    }
    /**
     * 标记一个项目里的通知为已读
     *
     * 标记一个项目里的通知为已读
     */
    @Test
    public void putV5ReposOwnerRepoNotificationsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.putV5ReposOwnerRepoNotifications(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * star 一个项目
     *
     * star 一个项目
     */
    @Test
    public void putV5UserStarredOwnerRepoTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.putV5UserStarredOwnerRepo(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * watch 一个项目
     *
     * watch 一个项目
     */
    @Test
    public void putV5UserSubscriptionsOwnerRepoTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.putV5UserSubscriptionsOwnerRepo(owner, repo, accessToken);

        // TODO: test validations
    }
}
