package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * RepositoriesCommitCommitTree
 */
public class RepositoriesCommitCommitTree   {
  @SerializedName("sha")
  private String sha = null;

  @SerializedName("url")
  private String url = null;

  public RepositoriesCommitCommitTree sha(String sha) {
    this.sha = sha;
    return this;
  }

   /**
   * Get sha
   * @return sha
  **/
  @ApiModelProperty(example = "62f4374a83ff9650a2f0b384a6fb296fc46c36cb", value = "")
  public String getSha() {
    return sha;
  }

  public void setSha(String sha) {
    this.sha = sha;
  }

  public RepositoriesCommitCommitTree url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RepositoriesCommitCommitTree repositoriesCommitCommitTree = (RepositoriesCommitCommitTree) o;
    return Objects.equals(this.sha, repositoriesCommitCommitTree.sha) &&
        Objects.equals(this.url, repositoriesCommitCommitTree.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sha, url);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RepositoriesCommitCommitTree {\n");
    
    sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

