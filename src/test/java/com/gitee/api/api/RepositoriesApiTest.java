package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.Branch;
import com.gitee.api.model.CommitContent;
import com.gitee.api.model.Compare;
import com.gitee.api.model.CompleteBranch;
import com.gitee.api.model.Content;
import com.gitee.api.model.Project;
import com.gitee.api.model.Release;
import com.gitee.api.model.RepoCommit;
import com.gitee.api.model.SSHKey;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for RepositoriesApi
 */
public class RepositoriesApiTest {

    private RepositoriesApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(RepositoriesApi.class);
    }

    /**
     * 删除一个项目
     *
     * 删除一个项目
     */
    @Test
    public void deleteV5ReposOwnerRepoTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepo(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 取消保护分支的设置
     *
     * 取消保护分支的设置
     */
    @Test
    public void deleteV5ReposOwnerRepoBranchesBranchProtectionTest() {
        String owner = null;
        String repo = null;
        String branch = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken);

        // TODO: test validations
    }
    /**
     * 移除项目成员
     *
     * 移除项目成员
     */
    @Test
    public void deleteV5ReposOwnerRepoCollaboratorsUsernameTest() {
        String owner = null;
        String repo = null;
        String username = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken);

        // TODO: test validations
    }
    /**
     * 删除Commit评论
     *
     * 删除Commit评论
     */
    @Test
    public void deleteV5ReposOwnerRepoCommentsIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 删除文件
     *
     * 删除文件
     */
    @Test
    public void deleteV5ReposOwnerRepoContentsPathTest() {
        String owner = null;
        String repo = null;
        String path = null;
        String sha = null;
        String message = null;
        String accessToken = null;
        String branch = null;
        String committerName = null;
        String committerEmail = null;
        String authorName = null;
        String authorEmail = null;
        // CommitContent response = api.deleteV5ReposOwnerRepoContentsPath(owner, repo, path, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail);

        // TODO: test validations
    }
    /**
     * 删除一个项目公钥
     *
     * 删除一个项目公钥
     */
    @Test
    public void deleteV5ReposOwnerRepoKeysIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoKeysId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 删除项目Release
     *
     * 删除项目Release
     */
    @Test
    public void deleteV5ReposOwnerRepoReleasesIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取一个组织的项目
     *
     * 获取一个组织的项目
     */
    @Test
    public void getV5OrgsOrgReposTest() {
        String org = null;
        String accessToken = null;
        String type = null;
        Integer page = null;
        Integer perPage = null;
        // Project response = api.getV5OrgsOrgRepos(org, accessToken, type, page, perPage);

        // TODO: test validations
    }
    /**
     * 列出授权用户的某个项目
     *
     * 列出授权用户的某个项目
     */
    @Test
    public void getV5ReposOwnerRepoTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepo(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 获取所有分支
     *
     * 获取所有分支
     */
    @Test
    public void getV5ReposOwnerRepoBranchesTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // java.util.List<Branch> response = api.getV5ReposOwnerRepoBranches(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 获取单个分支
     *
     * 获取单个分支
     */
    @Test
    public void getV5ReposOwnerRepoBranchesBranchTest() {
        String owner = null;
        String repo = null;
        String branch = null;
        String accessToken = null;
        // CompleteBranch response = api.getV5ReposOwnerRepoBranchesBranch(owner, repo, branch, accessToken);

        // TODO: test validations
    }
    /**
     * 获取项目的所有成员
     *
     * 获取项目的所有成员
     */
    @Test
    public void getV5ReposOwnerRepoCollaboratorsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoCollaborators(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 判断用户是否为项目成员
     *
     * 判断用户是否为项目成员
     */
    @Test
    public void getV5ReposOwnerRepoCollaboratorsUsernameTest() {
        String owner = null;
        String repo = null;
        String username = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken);

        // TODO: test validations
    }
    /**
     * 查看项目成员的权限
     *
     * 查看项目成员的权限
     */
    @Test
    public void getV5ReposOwnerRepoCollaboratorsUsernamePermissionTest() {
        String owner = null;
        String repo = null;
        String username = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoCollaboratorsUsernamePermission(owner, repo, username, accessToken);

        // TODO: test validations
    }
    /**
     * 获取项目的Commit评论
     *
     * 获取项目的Commit评论
     */
    @Test
    public void getV5ReposOwnerRepoCommentsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // Void response = api.getV5ReposOwnerRepoComments(owner, repo, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取项目的某条Commit评论
     *
     * 获取项目的某条Commit评论
     */
    @Test
    public void getV5ReposOwnerRepoCommentsIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 项目的所有提交
     *
     * 项目的所有提交
     */
    @Test
    public void getV5ReposOwnerRepoCommitsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String sha = null;
        String path = null;
        String author = null;
        String since = null;
        String until = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<RepoCommit> response = api.getV5ReposOwnerRepoCommits(owner, repo, accessToken, sha, path, author, since, until, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取单个Commit的评论
     *
     * 获取单个Commit的评论
     */
    @Test
    public void getV5ReposOwnerRepoCommitsRefCommentsTest() {
        String owner = null;
        String repo = null;
        String ref = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // Void response = api.getV5ReposOwnerRepoCommitsRefComments(owner, repo, ref, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 项目的某个提交
     *
     * 项目的某个提交
     */
    @Test
    public void getV5ReposOwnerRepoCommitsShaTest() {
        String owner = null;
        String repo = null;
        String sha = null;
        String accessToken = null;
        // RepoCommit response = api.getV5ReposOwnerRepoCommitsSha(owner, repo, sha, accessToken);

        // TODO: test validations
    }
    /**
     * 两个Commits之间对比的版本差异
     *
     * 两个Commits之间对比的版本差异
     */
    @Test
    public void getV5ReposOwnerRepoCompareBaseHeadTest() {
        String owner = null;
        String repo = null;
        String base = null;
        String head = null;
        String accessToken = null;
        // Compare response = api.getV5ReposOwnerRepoCompareBaseHead(owner, repo, base, head, accessToken);

        // TODO: test validations
    }
    /**
     * 获取仓库具体路径下的内容
     *
     * 获取仓库具体路径下的内容
     */
    @Test
    public void getV5ReposOwnerRepoContentsPathTest() {
        String owner = null;
        String repo = null;
        String path = null;
        String accessToken = null;
        String ref = null;
        // java.util.List<Content> response = api.getV5ReposOwnerRepoContentsPath(owner, repo, path, accessToken, ref);

        // TODO: test validations
    }
    /**
     * 获取项目贡献者
     *
     * 获取项目贡献者
     */
    @Test
    public void getV5ReposOwnerRepoContributorsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoContributors(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 查看项目的Forks
     *
     * 查看项目的Forks
     */
    @Test
    public void getV5ReposOwnerRepoForksTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String sort = null;
        Integer page = null;
        Integer perPage = null;
        // Void response = api.getV5ReposOwnerRepoForks(owner, repo, accessToken, sort, page, perPage);

        // TODO: test validations
    }
    /**
     * 展示项目的公钥
     *
     * 展示项目的公钥
     */
    @Test
    public void getV5ReposOwnerRepoKeysTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<SSHKey> response = api.getV5ReposOwnerRepoKeys(owner, repo, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取项目的单个公钥
     *
     * 获取项目的单个公钥
     */
    @Test
    public void getV5ReposOwnerRepoKeysIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // SSHKey response = api.getV5ReposOwnerRepoKeysId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取Pages信息
     *
     * 获取Pages信息
     */
    @Test
    public void getV5ReposOwnerRepoPagesTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoPages(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 获取仓库README
     *
     * 获取仓库README
     */
    @Test
    public void getV5ReposOwnerRepoReadmeTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String ref = null;
        // Content response = api.getV5ReposOwnerRepoReadme(owner, repo, accessToken, ref);

        // TODO: test validations
    }
    /**
     * 获取项目的所有Releases
     *
     * 获取项目的所有Releases
     */
    @Test
    public void getV5ReposOwnerRepoReleasesTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Release> response = api.getV5ReposOwnerRepoReleases(owner, repo, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取项目的单个Releases
     *
     * 获取项目的单个Releases
     */
    @Test
    public void getV5ReposOwnerRepoReleasesIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Release response = api.getV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取项目的最后更新的Release
     *
     * 获取项目的最后更新的Release
     */
    @Test
    public void getV5ReposOwnerRepoReleasesLatestTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Release response = api.getV5ReposOwnerRepoReleasesLatest(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 根据Tag名称获取项目的Release
     *
     * 根据Tag名称获取项目的Release
     */
    @Test
    public void getV5ReposOwnerRepoReleasesTagsTagTest() {
        String owner = null;
        String repo = null;
        String tag = null;
        String accessToken = null;
        // Release response = api.getV5ReposOwnerRepoReleasesTagsTag(owner, repo, tag, accessToken);

        // TODO: test validations
    }
    /**
     * 列出项目所有的tags
     *
     * 列出项目所有的tags
     */
    @Test
    public void getV5ReposOwnerRepoTagsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoTags(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 列出授权用户的所有项目
     *
     * 列出授权用户的所有项目
     */
    @Test
    public void getV5UserReposTest() {
        String accessToken = null;
        String visibility = null;
        String affiliation = null;
        String type = null;
        String sort = null;
        String direction = null;
        Integer page = null;
        Integer perPage = null;
        // Void response = api.getV5UserRepos(accessToken, visibility, affiliation, type, sort, direction, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取某个用户的公开项目
     *
     * 获取某个用户的公开项目
     */
    @Test
    public void getV5UsersUsernameReposTest() {
        String username = null;
        String accessToken = null;
        String type = null;
        String sort = null;
        String direction = null;
        Integer page = null;
        Integer perPage = null;
        // Void response = api.getV5UsersUsernameRepos(username, accessToken, type, sort, direction, page, perPage);

        // TODO: test validations
    }
    /**
     * 更新项目设置
     *
     * 更新项目设置
     */
    @Test
    public void patchV5ReposOwnerRepoTest() {
        String owner = null;
        String repo = null;
        String name = null;
        String accessToken = null;
        String description = null;
        String homepage = null;
        Boolean hasIssues = null;
        Boolean hasWiki = null;
        Boolean _private = null;
        String defaultBranch = null;
        // Void response = api.patchV5ReposOwnerRepo(owner, repo, name, accessToken, description, homepage, hasIssues, hasWiki, _private, defaultBranch);

        // TODO: test validations
    }
    /**
     * 更新Commit评论
     *
     * 更新Commit评论
     */
    @Test
    public void patchV5ReposOwnerRepoCommentsIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String body = null;
        String accessToken = null;
        // Void response = api.patchV5ReposOwnerRepoCommentsId(owner, repo, id, body, accessToken);

        // TODO: test validations
    }
    /**
     * 更新项目Release
     *
     * 更新项目Release
     */
    @Test
    public void patchV5ReposOwnerRepoReleasesIdTest() {
        String owner = null;
        String repo = null;
        String tagName = null;
        String name = null;
        String body = null;
        Integer id = null;
        String accessToken = null;
        Boolean prerelease = null;
        // Release response = api.patchV5ReposOwnerRepoReleasesId(owner, repo, tagName, name, body, id, accessToken, prerelease);

        // TODO: test validations
    }
    /**
     * 创建组织项目
     *
     * 创建组织项目
     */
    @Test
    public void postV5OrgsOrgReposTest() {
        String name = null;
        Integer org = null;
        String accessToken = null;
        String description = null;
        String homepage = null;
        Boolean hasIssues = null;
        Boolean hasWiki = null;
        Boolean _private = null;
        Boolean autoInit = null;
        String gitignoreTemplate = null;
        String licenseTemplate = null;
        // Void response = api.postV5OrgsOrgRepos(name, org, accessToken, description, homepage, hasIssues, hasWiki, _private, autoInit, gitignoreTemplate, licenseTemplate);

        // TODO: test validations
    }
    /**
     * 创建Commit评论
     *
     * 创建Commit评论
     */
    @Test
    public void postV5ReposOwnerRepoCommitsShaCommentsTest() {
        String owner = null;
        String repo = null;
        String sha = null;
        String body = null;
        String accessToken = null;
        String path = null;
        Integer position = null;
        // Void response = api.postV5ReposOwnerRepoCommitsShaComments(owner, repo, sha, body, accessToken, path, position);

        // TODO: test validations
    }
    /**
     * 新建文件
     *
     * 新建文件
     */
    @Test
    public void postV5ReposOwnerRepoContentsPathTest() {
        String owner = null;
        String repo = null;
        String path = null;
        String content = null;
        String message = null;
        String accessToken = null;
        String branch = null;
        String committerName = null;
        String committerEmail = null;
        String authorName = null;
        String authorEmail = null;
        // CommitContent response = api.postV5ReposOwnerRepoContentsPath(owner, repo, path, content, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail);

        // TODO: test validations
    }
    /**
     * Fork一个项目
     *
     * Fork一个项目
     */
    @Test
    public void postV5ReposOwnerRepoForksTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String organization = null;
        // Void response = api.postV5ReposOwnerRepoForks(owner, repo, accessToken, organization);

        // TODO: test validations
    }
    /**
     * 为项目添加公钥
     *
     * 为项目添加公钥
     */
    @Test
    public void postV5ReposOwnerRepoKeysTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // SSHKey response = api.postV5ReposOwnerRepoKeys(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 请求建立Pages
     *
     * 请求建立Pages
     */
    @Test
    public void postV5ReposOwnerRepoPagesBuildsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // Void response = api.postV5ReposOwnerRepoPagesBuilds(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 创建项目Release
     *
     * 创建项目Release
     */
    @Test
    public void postV5ReposOwnerRepoReleasesTest() {
        String owner = null;
        String repo = null;
        String tagName = null;
        String name = null;
        String body = null;
        String targetCommitish = null;
        String accessToken = null;
        Boolean prerelease = null;
        // Release response = api.postV5ReposOwnerRepoReleases(owner, repo, tagName, name, body, targetCommitish, accessToken, prerelease);

        // TODO: test validations
    }
    /**
     * 创建一个项目
     *
     * 创建一个项目
     */
    @Test
    public void postV5UserReposTest() {
        String name = null;
        String accessToken = null;
        String description = null;
        String homepage = null;
        Boolean hasIssues = null;
        Boolean hasWiki = null;
        Boolean _private = null;
        Boolean autoInit = null;
        String gitignoreTemplate = null;
        String licenseTemplate = null;
        // Void response = api.postV5UserRepos(name, accessToken, description, homepage, hasIssues, hasWiki, _private, autoInit, gitignoreTemplate, licenseTemplate);

        // TODO: test validations
    }
    /**
     * 设置分支保护
     *
     * 设置分支保护
     */
    @Test
    public void putV5ReposOwnerRepoBranchesBranchProtectionTest() {
        String owner = null;
        String repo = null;
        String branch = null;
        String accessToken = null;
        // CompleteBranch response = api.putV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken);

        // TODO: test validations
    }
    /**
     * 添加项目成员
     *
     * 添加项目成员
     */
    @Test
    public void putV5ReposOwnerRepoCollaboratorsUsernameTest() {
        String owner = null;
        String repo = null;
        String username = null;
        String permission = null;
        String accessToken = null;
        // Void response = api.putV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, permission, accessToken);

        // TODO: test validations
    }
    /**
     * 更新文件
     *
     * 更新文件
     */
    @Test
    public void putV5ReposOwnerRepoContentsPathTest() {
        String owner = null;
        String repo = null;
        String path = null;
        String content = null;
        String sha = null;
        String message = null;
        String accessToken = null;
        String branch = null;
        String committerName = null;
        String committerEmail = null;
        String authorName = null;
        String authorEmail = null;
        // CommitContent response = api.putV5ReposOwnerRepoContentsPath(owner, repo, path, content, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail);

        // TODO: test validations
    }
}
