
# GroupDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**login** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**reposUrl** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**membersUrl** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**location** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**_public** | **String** |  |  [optional]
**enterprise** | **String** |  |  [optional]
**members** | **String** |  |  [optional]
**publicRepos** | **String** |  |  [optional]
**privateRepos** | **String** |  |  [optional]
**owner** | **String** |  |  [optional]



