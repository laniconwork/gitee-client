package com.gitee.api.api;

import com.gitee.api.CollectionFormats.*;

import rx.Observable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import com.gitee.api.model.Group;
import com.gitee.api.model.GroupDetail;
import com.gitee.api.model.GroupMember;
import com.gitee.api.model.UserBasic;


public interface OrganizationsApi {
  /**
   * 移除授权用户所管理组织中的成员
   * 移除授权用户所管理组织中的成员
   * @param org 组织的路径(path/login) (required)
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/orgs/{org}/memberships/{username}")
  Observable<Void> deleteV5OrgsOrgMembershipsUsername(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 退出一个组织
   * 退出一个组织
   * @param org 组织的路径(path/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/user/memberships/orgs/{org}")
  Observable<Void> deleteV5UserMembershipsOrgsOrg(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取一个组织
   * 获取一个组织
   * @param org 组织的路径(path/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Group&gt;
   */
  @GET("v5/orgs/{org}")
  Observable<Group> getV5OrgsOrg(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出一个组织的所有成员
   * 列出一个组织的所有成员
   * @param org 组织的路径(path/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @param role 根据角色筛选成员 (optional, default to all)
   * @return Call&lt;java.util.List&lt;UserBasic&gt;&gt;
   */
  @GET("v5/orgs/{org}/members")
  Observable<java.util.List<UserBasic>> getV5OrgsOrgMembers(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage, @retrofit2.http.Query("role") String role
  );

  /**
   * 获取授权用户所属组织的一个成员
   * 获取授权用户所属组织的一个成员
   * @param org 组织的路径(path/login) (required)
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;GroupMember&gt;
   */
  @GET("v5/orgs/{org}/memberships/{username}")
  Observable<GroupMember> getV5OrgsOrgMembershipsUsername(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出授权用户在所属组织的成员资料
   * 列出授权用户在所属组织的成员资料
   * @param accessToken 用户授权码 (optional)
   * @param active 根据成员是否已激活进行筛选资料，缺省返回所有资料 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;GroupMember&gt;&gt;
   */
  @GET("v5/user/memberships/orgs")
  Observable<java.util.List<GroupMember>> getV5UserMembershipsOrgs(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("active") Boolean active, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取授权用户在一个组织的成员资料
   * 获取授权用户在一个组织的成员资料
   * @param org 组织的路径(path/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;GroupMember&gt;
   */
  @GET("v5/user/memberships/orgs/{org}")
  Observable<GroupMember> getV5UserMembershipsOrgsOrg(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出授权用户所属的组织
   * 列出授权用户所属的组织
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @param admin 只列出授权用户管理的组织 (optional)
   * @return Call&lt;java.util.List&lt;Group&gt;&gt;
   */
  @GET("v5/user/orgs")
  Observable<java.util.List<Group>> getV5UserOrgs(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage, @retrofit2.http.Query("admin") Boolean admin
  );

  /**
   * 列出用户所属的组织
   * 列出用户所属的组织
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Group&gt;&gt;
   */
  @GET("v5/users/{username}/orgs")
  Observable<java.util.List<Group>> getV5UsersUsernameOrgs(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 更新授权用户所管理的组织资料
   * 更新授权用户所管理的组织资料
   * @param org 组织的路径(path/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param email 组织公开的邮箱地址 (optional)
   * @param location 组织所在地 (optional)
   * @param name 组织名称 (optional)
   * @param description 组织简介 (optional)
   * @param htmlUrl 组织站点 (optional)
   * @return Call&lt;GroupDetail&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/orgs/{org}")
  Observable<GroupDetail> patchV5OrgsOrg(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("email") String email, @retrofit2.http.Field("location") String location, @retrofit2.http.Field("name") String name, @retrofit2.http.Field("description") String description, @retrofit2.http.Field("html_url") String htmlUrl
  );

  /**
   * 更新授权用户在一个组织的成员资料
   * 更新授权用户在一个组织的成员资料
   * @param org 组织的路径(path/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param remark 在组织中的备注信息 (optional)
   * @return Call&lt;GroupMember&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/user/memberships/orgs/{org}")
  Observable<GroupMember> patchV5UserMembershipsOrgsOrg(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("remark") String remark
  );

  /**
   * 增加或更新授权用户所管理组织的成员
   * 增加或更新授权用户所管理组织的成员
   * @param org 组织的路径(path/login) (required)
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param role 设置用户在组织的角色 (optional, default to member)
   * @return Call&lt;GroupMember&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/orgs/{org}/memberships/{username}")
  Observable<GroupMember> putV5OrgsOrgMembershipsUsername(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Path("username") String username, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("role") String role
  );

}
