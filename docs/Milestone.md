
# Milestone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**number** | **Integer** |  |  [optional]
**repositoryId** | **Integer** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**updatedAt** | [**java.util.Date**](java.util.Date.md) |  |  [optional]
**createdAt** | [**java.util.Date**](java.util.Date.md) |  |  [optional]
**openIssues** | **Integer** |  |  [optional]
**startedIssues** | **Integer** |  |  [optional]
**closedIssues** | **Integer** |  |  [optional]
**approvedIssues** | **Integer** |  |  [optional]
**dueOn** | [**java.util.Date**](java.util.Date.md) |  |  [optional]



