# GitDataApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getV5ReposOwnerRepoGitBlobsSha**](GitDataApi.md#getV5ReposOwnerRepoGitBlobsSha) | **GET** v5/repos/{owner}/{repo}/git/blobs/{sha} | 获取文件Blob
[**getV5ReposOwnerRepoGitTreesSha**](GitDataApi.md#getV5ReposOwnerRepoGitTreesSha) | **GET** v5/repos/{owner}/{repo}/git/trees/{sha} | 获取目录Tree


<a name="getV5ReposOwnerRepoGitBlobsSha"></a>
# **getV5ReposOwnerRepoGitBlobsSha**
> Blob getV5ReposOwnerRepoGitBlobsSha(owner, repo, sha, accessToken)

获取文件Blob

获取文件Blob

### Example
```java
// Import classes:
//import com.gitee.api.api.GitDataApi;

GitDataApi apiInstance =  new ApiClient().create(GitDataApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String sha = "sha_example"; // String | 文件Blob的SHA值
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Blob> result = apiInstance.getV5ReposOwnerRepoGitBlobsSha(owner, repo, sha, accessToken);
result.subscribe(new Observer<Blob>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Blob response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **sha** | **String**| 文件Blob的SHA值 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Blob**](Blob.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoGitTreesSha"></a>
# **getV5ReposOwnerRepoGitTreesSha**
> Tree getV5ReposOwnerRepoGitTreesSha(owner, repo, sha, accessToken, recursive)

获取目录Tree

获取目录Tree

### Example
```java
// Import classes:
//import com.gitee.api.api.GitDataApi;

GitDataApi apiInstance =  new ApiClient().create(GitDataApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String sha = "sha_example"; // String | 可以是分支名(如master)、Commit或者目录Tree的SHA值
String accessToken = "accessToken_example"; // String | 用户授权码
Integer recursive = 56; // Integer | 赋值为1递归获取目录
Observable<Tree> result = apiInstance.getV5ReposOwnerRepoGitTreesSha(owner, repo, sha, accessToken, recursive);
result.subscribe(new Observer<Tree>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Tree response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **sha** | **String**| 可以是分支名(如master)、Commit或者目录Tree的SHA值 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **recursive** | **Integer**| 赋值为1递归获取目录 | [optional]

### Return type

[**Tree**](Tree.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

