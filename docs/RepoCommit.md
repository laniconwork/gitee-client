
# RepoCommit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  |  [optional]
**sha** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**commentsUrl** | **String** |  |  [optional]
**commit** | **String** |  |  [optional]
**author** | **String** |  |  [optional]
**committer** | **String** |  |  [optional]
**parents** | **String** |  |  [optional]
**stats** | **String** |  |  [optional]



